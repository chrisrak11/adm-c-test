export interface ISortingPref {
    ascending: boolean,
    param: string
}
