import { ISortingPref } from './../types/ISortingPref';
import Vue from 'vue'
import Vuex from 'vuex'
import { ListStoreProductsApi } from '@/services/ListStoreProductsApi';
import { IListStoreProduct } from '@/domain/IListStoreProduct';
import { DetailStoreProductsApi } from '@/services/DetailStoreProductsApi';
import { IDetailStoreProduct } from '@/domain/IDetailStoreProduct';
import { IDetailStoreProductAvailability } from '@/domain/IDetailStoreProductAvailability';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        listStoreProducts: [] as IListStoreProduct[],
        detailStoreProducts: [] as IDetailStoreProduct[],
        detailStoreProduct: null as (IDetailStoreProduct | null),
        detailStoreProductsAvailabilities: [] as IDetailStoreProductAvailability[],
        sortingPref: { ascending: true, param: 'none' } as ISortingPref
    },
    mutations: {
        setListStoreProducts(state, listStoreProducts: IListStoreProduct[]) {
            state.listStoreProducts = listStoreProducts;
        },
        setDetailStoreProductsAvailabilities(state, detailStoreProductsAvailabilities: IDetailStoreProductAvailability[]) {
            state.detailStoreProductsAvailabilities = detailStoreProductsAvailabilities;
        },
        setCurrentDetailStoreProduct(state, detailStoreProduct: IDetailStoreProduct) {
            state.detailStoreProduct = detailStoreProduct;
        },
        setSortingPref(state, sortingPref: ISortingPref) {
            state.sortingPref = sortingPref;
        }
    },
    getters: {
        availabilityForProduct: (context) => (id: string) => {
            return context.detailStoreProductsAvailabilities.find(prod => prod.id === id)?.availability
        },

        listProductsSorted(context): IListStoreProduct[] {
            if (context.sortingPref.param === 'none') {
                return context.listStoreProducts;
            }
            if (context.sortingPref.param === 'popularity') {
                if (context.sortingPref.ascending) {
                    return context.listStoreProducts.slice().sort((a, b) => (a.sorting.popular > b.sorting.popular) ? 1 : -1);
                } else {
                    return context.listStoreProducts.slice().sort((a, b) => (a.sorting.popular < b.sorting.popular) ? 1 : -1);
                }
            }
            if (context.sortingPref.param === 'price') {
                if (context.sortingPref.ascending) {
                    return context.listStoreProducts.slice().sort((a, b) => (a.price > b.price) ? 1 : -1);
                } else {
                    return context.listStoreProducts.slice().sort((a, b) => (a.price < b.price) ? 1 : -1);
                }
            }
            return context.listStoreProducts;
        },

        isSortingDirectionAscending(context): boolean {
            return context.sortingPref.ascending
        },

        isSortingParamMatching: (context) => (param: string) => {
            return context.sortingPref.param === param
        }
    },
    actions: {
        async getListStoreProducts(context): Promise<void> {
            const listStoreProducts = await ListStoreProductsApi.getAll();
            context.commit('setListStoreProducts', listStoreProducts);
        },
        async getDetailStoreProduct(context, id: string): Promise<void> {
            const currentDetailStoreProduct = await DetailStoreProductsApi.getSingle(id);
            context.commit('setCurrentDetailStoreProduct', currentDetailStoreProduct);
        },
        async getDetailStoreProductsAvailabilities(context): Promise<void> {
            const detailStoreProductsAvailabilities = await DetailStoreProductsApi.getAvailabilitiesForAll();
            context.commit('setDetailStoreProductsAvailabilities', detailStoreProductsAvailabilities);
        },
        sortByParam(context, param: string): void {
            const pref = context.state.sortingPref
            pref.ascending = true;
            pref.param = param;
            context.commit('setSortingPref', pref);
        },
        sortByDirection(context, isAscending: boolean): void {
            if (context.state.sortingPref.param === 'none') return;
            const pref = context.state.sortingPref
            pref.ascending = isAscending;
            context.commit('setSortingPref', pref);
        }
    },
    modules: {
    }
})
