import { IListStoreProductSorting } from './IListStoreProductSorting';
export interface IListStoreProduct {
    title: string,
    description: string,
    image: string,
    price: number,
    sorting: IListStoreProductSorting,
    id: string,
}
