export interface IDetailStoreProductAvailability {
    id: string,
    availability: string
}
