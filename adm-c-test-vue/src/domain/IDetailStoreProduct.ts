export interface IDetailStoreProduct {
    title: string,
    description: string,
    image: string,
    specs: string[],
    availability: string,
    id: string,
    price: number
}
