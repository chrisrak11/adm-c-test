import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import StoreProductsIndex from '../views/StoreProducts/Index.vue'
import StoreProductsDetails from '../views/StoreProducts/Details.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    { path: '/', name: 'Home', component: Home, meta: { title: 'Home' } },

    { path: '/Products/', name: 'StoreProducts', component: StoreProductsIndex, meta: { title: 'Products' } },
    { path: '/Products/Details/:id', name: 'StoreProductsDetails', component: StoreProductsDetails, props: true, meta: { title: 'Product details' } }
]

const router = new VueRouter({
    routes
})

router.afterEach((to, from) => {
    if (to.meta && to.meta.title) {
        document.title = to.meta.title + ' | C# Task - Chris Rak';
    }
});

export default router
