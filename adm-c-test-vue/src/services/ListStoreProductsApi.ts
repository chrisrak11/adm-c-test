import { IListStoreProduct } from './../domain/IListStoreProduct';

import Axios from 'axios';

export abstract class ListStoreProductsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://adm-c-test-aspnet.gustav-solutions.eu/api/v1.0/ListStoreProducts/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    );

    static async getAll(): Promise<IListStoreProduct[]> {
        const url = "";
        try {
            const response = await this.axios.get<IListStoreProduct[]>(url);
            if (response.status === 200) {
                return this.sanitize(response.data);
            }
            return [];
        } catch (error) {
            console.log("error", (error as Error).message);
            return [];
        }
    }

    private static sanitize(listStoreProducts: IListStoreProduct[]): IListStoreProduct[] {
        listStoreProducts.forEach(prod => {
            prod.description = prod.description.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/&lt;b&gt;/g, "<b>").replace(/&lt;\/b&gt;/g, "</b>").replace(/&lt;i&gt;/g, "<i>").replace(/&lt;\/i&gt;/g, "</i>");
        });
        return listStoreProducts
    }
}
