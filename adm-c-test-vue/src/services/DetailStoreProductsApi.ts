import { IDetailStoreProduct } from './../domain/IDetailStoreProduct';
import { IDetailStoreProductAvailability } from './../domain/IDetailStoreProductAvailability';

import Axios from 'axios';

export abstract class DetailStoreProductsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://adm-c-test-aspnet.gustav-solutions.eu/api/v1.0/DetailStoreProducts/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    );

    static async getAll(): Promise<IDetailStoreProduct[]> {
        const url = "";
        try {
            const response = await this.axios.get<IDetailStoreProduct[]>(url);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log("error", (error as Error).message);
            return [];
        }
    }

    static async getSingle(id: string): Promise<IDetailStoreProduct | null> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IDetailStoreProduct>(url);
            if (response.status === 200) {
                return this.sanitize(response.data);
            }
            return null;
        } catch (error) {
            console.log("error", (error as Error).message);
            return null;
        }
    }

    static async getAvailabilitiesForAll(): Promise<IDetailStoreProductAvailability[]> {
        const url = "Availabilities";
        try {
            const response = await this.axios.get<IDetailStoreProductAvailability[]>(url);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log("error", (error as Error).message);
            return [];
        }
    }

    private static sanitize(detailStoreProduct: IDetailStoreProduct): IDetailStoreProduct {
        detailStoreProduct.description = detailStoreProduct.description.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/&lt;b&gt;/g, "<b>").replace(/&lt;\/b&gt;/g, "</b>").replace(/&lt;i&gt;/g, "<i>").replace(/&lt;\/i&gt;/g, "</i>");
        return detailStoreProduct;
    }
}
