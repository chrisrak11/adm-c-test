using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicAPI.DTO.v1._1._0;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Products list
    /// </summary>
    [ApiController]
    [ApiVersion( "1.0" )]
    [Route("api/v{version:apiVersion}/[controller]")]
    
    public class ListStoreProductsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ListStoreProductMapper _mapper = new ListStoreProductMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public ListStoreProductsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/ListStoreProducts
        /// <summary>
        /// Get all ListStoreProducts.
        /// </summary>
        /// <returns>Array of ListStoreProducts</returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ListStoreProduct>))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<IEnumerable<ListStoreProduct>>> GetListStoreProducts()
        {
            
            return Ok((await _bll.ListStoreProducts.GetAllAsyncAsyncSanitized()).Select(e => _mapper.Map(e)));
        }
        
    }
}