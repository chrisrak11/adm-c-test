using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicAPI.DTO.v1._1._0;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Products' details
    /// </summary>
    [ApiController]
    [ApiVersion( "1.0" )]
    [Route("api/v{version:apiVersion}/[controller]")]
    
    public class DetailStoreProductsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly DetailStoreProductMapper _mapper = new DetailStoreProductMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public DetailStoreProductsController(IAppBLL bll)
        {
            _bll = bll;
        }
        

        // GET: api/DetailStoreProducts/5
        /// <summary>
        /// Get a specific DetailStoreProduct.
        /// </summary>
        /// <param name="id">DetailStoreProduct's ID</param>
        /// <returns>DetailStoreProduct object</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DetailStoreProduct))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<DetailStoreProduct>> GetDetailStoreProduct(string id)
        {
            var detailStoreProduct = await _bll.DetailStoreProducts.FirstOrDefaultAsyncSanitized(id);

            if (detailStoreProduct == null)
            {
                return NotFound(new MessageDTO($"A product with the ID {id} was not found."));
            }
            
            return Ok(_mapper.Map(detailStoreProduct));
        }
        
        // GET: api/DetailStoreProducts/Availabilities/
        /// <summary>
        /// Get all DetailStoreProductAvailabilities.
        /// </summary>
        /// <returns>Array of DetailStoreProductAvailabilities</returns>
        [HttpGet("Availabilities")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DetailStoreProductAvailability))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageDTO))]
        public async Task<ActionResult<IEnumerable<DetailStoreProductAvailability>>> GetDetailStoreProductAvailability()
        {
            var availabilityMapper = new DetailStoreProductAvailabilityMapper();
            var detailStoreProducts = await _bll.DetailStoreProducts.GetAllProductAvailabilitiesAsync();
            return Ok(detailStoreProducts.Select(e => availabilityMapper.Map(e)));
        }
        
    }
}