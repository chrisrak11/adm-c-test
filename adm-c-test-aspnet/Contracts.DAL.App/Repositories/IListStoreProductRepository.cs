using DAL.App.DTO;
using ee.itcollege.chrrak.Contracts.DAL.Base.Repositories;

namespace Contracts.DAL.App.Repositories
{
    public interface IListStoreProductRepository : IBaseRepository<string, ListStoreProduct>, IListStoreProductRepositoryCustom
    {
        
    }
}