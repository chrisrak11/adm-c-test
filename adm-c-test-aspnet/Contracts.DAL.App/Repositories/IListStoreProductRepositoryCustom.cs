using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IListStoreProductRepositoryCustom : IListStoreProductRepositoryCustom<ListStoreProduct>
    {
        
    }
    
    public interface IListStoreProductRepositoryCustom<TListStoreProduct>
    {
        
    }
}