using DAL.App.DTO;
using ee.itcollege.chrrak.Contracts.DAL.Base.Repositories;

namespace Contracts.DAL.App.Repositories
{
    public interface IDetailStoreProductRepository : IBaseRepository<string, DetailStoreProduct>, IDetailStoreProductRepositoryCustom
    {
        
    }
}