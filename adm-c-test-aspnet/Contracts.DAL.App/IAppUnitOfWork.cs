﻿using System;
using Contracts.DAL.App.Repositories;
using ee.itcollege.chrrak.Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork, IBaseEntityTracker<string>
    {
        IListStoreProductRepository ListStoreProducts { get; }
        IDetailStoreProductRepository DetailStoreProducts { get; }
    }
}