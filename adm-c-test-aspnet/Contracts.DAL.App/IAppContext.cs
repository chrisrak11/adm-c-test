using System.Threading.Tasks;
using Domain.App;

namespace Contracts.DAL.App
{
    public interface IAppContext
    {
        Task<ListStore> GetListStore();
        Task<DetailStore> GetDetailStore();
    }
}