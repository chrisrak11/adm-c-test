using System;

namespace Domain.App
{
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ListStoreProductSorting
    {
        public int Popular { get; set; }
    }
}