using System;

namespace Domain.App
{
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class ListStore
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("Product", IsNullable = false)]
        public ListStoreProduct[] Products { get; set; } = default!;
    }
}