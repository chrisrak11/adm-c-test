using System;
using System.Collections.Generic;
using System.Text;
using ee.itcollege.chrrak.Contracts.Domain.Base;

namespace Domain.App
{
    [SerializableAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ListStoreProduct : IDomainEntityId<string>
    {
        public string Title { get; set; } = default!;
        public string Description { get; set; } = default!;
        public string Image { get; set; } = default!;
        public decimal Price { get; set; }
        public ListStoreProductSorting Sorting { get; set; } = default!;
        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        public string Id { get; set; } = default!;
    }
}
