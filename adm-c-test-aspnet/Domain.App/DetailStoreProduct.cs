using System;
using ee.itcollege.chrrak.Contracts.Domain.Base;

namespace Domain.App
{
    [SerializableAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class DetailStoreProduct : IDomainEntityId<string>
    {
        public string Title { get; set; } = default!;
        public string Description { get; set; } = default!;
        public string Image { get; set; } = default!;
        [System.Xml.Serialization.XmlArrayItemAttribute("Spec", IsNullable = false)]
        public string[] Specs { get; set; } = default!;
        public string Availability { get; set; } = default!;
        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        public string Id { get; set; } = default!;
    }


}
