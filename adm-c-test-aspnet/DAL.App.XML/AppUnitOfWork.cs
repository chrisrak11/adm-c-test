using System;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.XML.Repositories;
using ee.itcollege.chrrak.Contracts.Domain.Base;
using ee.itcollege.chrrak.DAL.Base;

namespace DAL.App.XML
{
    public class AppUnitOfWork : BaseUnitOfWork<string>, IAppUnitOfWork
    {
        public IAppContext AppContext { get; }
        public AppUnitOfWork(IAppContext appContext)
        {
            AppContext = appContext;
        }
        

        public IListStoreProductRepository ListStoreProducts =>
            GetRepository<IListStoreProductRepository>(() => new ListStoreProductRepository(AppContext));

        public IDetailStoreProductRepository DetailStoreProducts =>
            GetRepository<IDetailStoreProductRepository>(() => new DetailStoreProductRepository(AppContext));

        public override Task<int> SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        public void AddToEntityTracker(IDomainEntityId<Guid> internalEntity, IDomainEntityId<Guid> externalEntity)
        {
            throw new NotImplementedException();
        }
    }
}