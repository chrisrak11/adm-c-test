using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.XML.Mappers;
using ee.itcollege.chrrak.Contracts.DAL.Base.Repositories;

namespace DAL.App.XML.Repositories
{
    public class ListStoreProductRepository : IListStoreProductRepository
    {
        private IAppContext AppContext { get; set; }

        private DALMapper<Domain.App.ListStoreProduct, DAL.App.DTO.ListStoreProduct> _mapper =
            new DALMapper<Domain.App.ListStoreProduct, DAL.App.DTO.ListStoreProduct>();
        public ListStoreProductRepository(IAppContext appContext)
        {
            AppContext = appContext;
        }
        
        public async Task<IEnumerable<ListStoreProduct>> GetAllAsync(object? userId = null, bool noTracking = true)
        {
            return (await AppContext.GetListStore()).Products.Select(prod => _mapper.Map(prod));
        }

        public Task<ListStoreProduct> FirstOrDefaultAsync(string id, object? userId = null, bool noTracking = true)
        {
            throw new System.NotImplementedException();
        }


        public ListStoreProduct Add(ListStoreProduct entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<ListStoreProduct> UpdateAsync(ListStoreProduct entity, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<ListStoreProduct> RemoveAsync(ListStoreProduct entity, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<ListStoreProduct> RemoveAsync(string id, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> ExistsAsync(string id, object? userId = null)
        {
            throw new System.NotImplementedException();
        }
    }
}