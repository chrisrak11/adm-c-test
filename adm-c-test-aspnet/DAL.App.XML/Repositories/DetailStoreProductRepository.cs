using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.XML.Mappers;

namespace DAL.App.XML.Repositories
{
    public class DetailStoreProductRepository : IDetailStoreProductRepository
    {
        public IAppContext AppContext { get; set; }
        private DALMapper<Domain.App.ListStoreProduct, DAL.App.DTO.ListStoreProduct> _listStoreProductMapper =
            new DALMapper<Domain.App.ListStoreProduct, DAL.App.DTO.ListStoreProduct>();
        private DALMapper<Domain.App.DetailStoreProduct, DAL.App.DTO.DetailStoreProduct> _detailStoreProductMapper =
            new DALMapper<Domain.App.DetailStoreProduct, DAL.App.DTO.DetailStoreProduct>();
        public DetailStoreProductRepository(IAppContext appContext)
        {
            AppContext = appContext;
        }
        
        public async Task<IEnumerable<DetailStoreProduct>> GetAllAsync(object? userId = null, bool noTracking = true)
        {
            return (await AppContext.GetDetailStore()).Products.Select(prod => _detailStoreProductMapper.Map(prod));
        }

        public async Task<DetailStoreProduct> FirstOrDefaultAsync(string id, object? userId = null, bool noTracking = true)
        {
            var listStoreProducts = (await AppContext.GetListStore()).Products.Select(prod => _listStoreProductMapper.Map(prod));
            var detailStoreProducts = (await AppContext.GetDetailStore()).Products.Select(prod => _detailStoreProductMapper.Map(prod));

            
            var query = detailStoreProducts.Join(
                listStoreProducts,
                detailProduct => detailProduct.Id,
                listProduct => listProduct.Id,
                (detailProduct, listProduct) => new DetailStoreProduct()
                {
                    Availability = detailProduct.Availability,
                    Description = detailProduct.Description,
                    Id = detailProduct.Id,
                    Image = detailProduct.Image,
                    Specs = detailProduct.Specs,
                    Title = detailProduct.Title,
                    Price = listProduct.Price
                }
            ).FirstOrDefault(detailAndListProduct => detailAndListProduct.Id.Equals(id));;
            return query;
            
        }

        public DetailStoreProduct Add(DetailStoreProduct entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<DetailStoreProduct> UpdateAsync(DetailStoreProduct entity, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<DetailStoreProduct> RemoveAsync(DetailStoreProduct entity, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<DetailStoreProduct> RemoveAsync(string id, object? userId = null)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> ExistsAsync(string id, object? userId = null)
        {
            throw new System.NotImplementedException();
        }
        
    }
}