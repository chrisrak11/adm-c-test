﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Contracts.DAL.App;
using Domain.App;

namespace DAL.App.XML
{
    public class AppContext : IAppContext
    {
        public AppContext(/*string listXmlFilePath, string detailXmlFilePath*/)
        {
            
        }

        public async Task<ListStore> GetListStore()
        {
            ListStore listStore = new ListStore();
            await Task.Run(() =>
            {
                using var filestream = System.IO.File.Open(@"List.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
                XmlSerializer serializerListXml = new XmlSerializer(typeof(Domain.App.ListStore), new XmlRootAttribute("Store"));
                var data = (Domain.App.ListStore)serializerListXml.Deserialize(filestream);
                filestream.Close();
                listStore = data;
            });


            return listStore;
        }
        
        public async Task<DetailStore> GetDetailStore()
        {
            DetailStore detailStore = new DetailStore();
            await Task.Run(() =>
            {
                using var filestream = System.IO.File.Open(@"Detail.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
                XmlSerializer serializerListXml = new XmlSerializer(typeof(Domain.App.DetailStore), new XmlRootAttribute("Store"));
                var data = (Domain.App.DetailStore)serializerListXml.Deserialize(filestream);
                filestream.Close();
                detailStore = data;
            });


            return detailStore;
        }
        
    }
}