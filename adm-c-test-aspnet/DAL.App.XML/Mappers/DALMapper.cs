using AutoMapper;
using ee.itcollege.chrrak.DAL.Base.Mappers;

namespace DAL.App.XML.Mappers
{
    public class DALMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
    where TRightObject : class? , new()
    where TLeftObject : class?, new()
    {
        public DALMapper()
        {
            MapperConfigurationExpression
                .CreateMap<Domain.App.ListStore, DAL.App.DTO.ListStore>();
            MapperConfigurationExpression
                .CreateMap<Domain.App.ListStoreProduct, DAL.App.DTO.ListStoreProduct>();
            MapperConfigurationExpression
                .CreateMap<Domain.App.ListStoreProductSorting, DAL.App.DTO.ListStoreProductSorting>();
            MapperConfigurationExpression
                .CreateMap<Domain.App.DetailStore, DAL.App.DTO.DetailStore>();
            MapperConfigurationExpression
                .CreateMap<Domain.App.DetailStoreProduct, DAL.App.DTO.DetailStoreProduct>();
            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}