using System;

namespace DAL.App.DTO
{
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class DetailStore
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("Product", IsNullable = false)]
        public DetailStoreProduct[] Products { get; set; } = default!;
    }
}