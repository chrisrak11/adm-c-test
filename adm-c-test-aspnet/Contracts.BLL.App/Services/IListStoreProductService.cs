using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.DAL.App.Repositories;
using ee.itcollege.chrrak.Contracts.BLL.Base.Services;

namespace Contracts.BLL.App.Services
{
    public interface IListStoreProductService : IListStoreProductRepositoryCustom<ListStoreProduct>, IBaseEntityService<string, ListStoreProduct>
    {
        Task<IEnumerable<ListStoreProduct>> GetAllAsyncAsyncSanitized(object? userId = null, bool noTracking = true);
    }
}