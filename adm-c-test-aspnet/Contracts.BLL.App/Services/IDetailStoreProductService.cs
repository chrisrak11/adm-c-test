using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.DAL.App.Repositories;
using ee.itcollege.chrrak.Contracts.BLL.Base.Services;
using ee.itcollege.chrrak.Contracts.DAL.Base.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IDetailStoreProductService : IDetailStoreProductRepositoryCustom<DetailStoreProduct>, IBaseEntityService<string, DetailStoreProduct>
    {
        Task<DetailStoreProduct> FirstOrDefaultAsyncSanitized(string detailStoreProductId, object? userId = null,
            bool noTracking = true);
        
        Task<IEnumerable<DetailStoreProductAvailability>> GetAllProductAvailabilitiesAsync(object? userId = null,
            bool noTracking = true);
    }
}