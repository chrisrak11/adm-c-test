using Contracts.BLL.App.Services;

namespace Contracts.BLL.App
{
    public interface IAppBLL
    {
        public IListStoreProductService ListStoreProducts { get; }
        public IDetailStoreProductService DetailStoreProducts { get; }
    }
}