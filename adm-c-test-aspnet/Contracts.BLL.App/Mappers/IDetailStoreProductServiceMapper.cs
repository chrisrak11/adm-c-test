using ee.itcollege.chrrak.Contracts.BLL.Base.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IDetailStoreProductServiceMapper : IBaseMapper<DALAppDTO.DetailStoreProduct, BLLAppDTO.DetailStoreProduct>
    {
    }
}