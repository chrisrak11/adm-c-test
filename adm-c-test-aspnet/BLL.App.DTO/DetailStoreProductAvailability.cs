namespace BLL.App.DTO
{
    public class DetailStoreProductAvailability
    {
        public string Id { get; set; } = default!;
        public string Availability { get; set; } = default!;
    }
}