using System;

namespace BLL.App.DTO
{
    [Serializable]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ListStoreProductSorting
    {
        public int Popular { get; set; }
    }
}