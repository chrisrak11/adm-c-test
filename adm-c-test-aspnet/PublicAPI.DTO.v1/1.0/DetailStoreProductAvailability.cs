namespace PublicAPI.DTO.v1._1._0
{
    public class DetailStoreProductAvailability
    {
        public string Id { get; set; } = default!;
        public string Availability { get; set; } = default!;
    }
}