﻿namespace PublicAPI.DTO.v1._1._0
{
    public class ListStoreProduct
    {
        public string Title { get; set; } = default!;
        public string Description { get; set; } = default!;
        public string Image { get; set; } = default!;
        public decimal Price { get; set; }
        public ListStoreProductSorting Sorting { get; set; } = default!;
        public string Id { get; set; } = default!;
    }
}