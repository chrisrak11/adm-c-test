﻿namespace PublicAPI.DTO.v1._1._0
{
    public class DetailStoreProduct
    {
        public string Title { get; set; } = default!;
        public string Description { get; set; } = default!;
        public string Image { get; set; } = default!;
        public string[] Specs { get; set; } = default!;
        public string Availability { get; set; } = default!;
        public decimal Price { get; set; }
        public string Id { get; set; } = default!;
    }
}