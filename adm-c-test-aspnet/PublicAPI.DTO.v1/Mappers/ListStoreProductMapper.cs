using AutoMapper;
using PublicAPI.DTO.v1._1._0;


namespace PublicApi.DTO.v1.Mappers
{
    public class ListStoreProductMapper : BaseMapper<BLL.App.DTO.ListStoreProduct, ListStoreProduct>
    {
        public ListStoreProductMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ListStoreProductSorting, ListStoreProductSorting>();
            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}