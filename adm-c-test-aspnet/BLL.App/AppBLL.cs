﻿using System;
using BLL.App.Services;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using ee.itcollege.chrrak.BLL.Base;

namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        public AppBLL(IAppUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IListStoreProductService ListStoreProducts =>
            GetService<IListStoreProductService>(() => new ListStoreProductService(UnitOfWork));
        public IDetailStoreProductService DetailStoreProducts => GetService<IDetailStoreProductService>(() => new DetailStoreProductService(UnitOfWork));
    }
}