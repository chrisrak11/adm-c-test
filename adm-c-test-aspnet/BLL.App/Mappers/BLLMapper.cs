using AutoMapper;
using ee.itcollege.chrrak.BLL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class BLLMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
    where TRightObject : class?, new()
    where TLeftObject : class?, new()
    {
        public BLLMapper()
        {
            /*MapperConfigurationExpression.CreateMap<DAL.App.DTO.ListStore, BLL.App.DTO.ListStore>();*/
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ListStoreProduct, BLL.App.DTO.ListStoreProduct>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ListStoreProductSorting, BLL.App.DTO.ListStoreProductSorting>();

            /*MapperConfigurationExpression.CreateMap<DAL.App.DTO.DetailStore, BLL.App.DTO.DetailStore>();*/
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DetailStoreProduct, BLL.App.DTO.DetailStoreProduct>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DetailStoreProduct, BLL.App.DTO.DetailStoreProductAvailability>();
            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
            
        }
    }
}