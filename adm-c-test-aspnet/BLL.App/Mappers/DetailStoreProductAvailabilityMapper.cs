using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace BLL.App.Mappers
{
    public class DetailStoreProductAvailabilityMapper : BLLMapper<DALAppDTO.DetailStoreProduct, BLLAppDTO.DetailStoreProductAvailability>
    {
        
    }
}