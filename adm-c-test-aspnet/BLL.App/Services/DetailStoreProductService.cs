using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using ee.itcollege.chrrak.BLL.Base.Services;


namespace BLL.App.Services
{
    public class DetailStoreProductService : BaseEntityService<string, IAppUnitOfWork, IDetailStoreProductRepository, IDetailStoreProductServiceMapper, DAL.App.DTO.DetailStoreProduct, BLL.App.DTO.DetailStoreProduct>, IDetailStoreProductService
    {
        public DetailStoreProductService(IAppUnitOfWork uow) : base(uow, uow.DetailStoreProducts, new DetailStoreProductServiceMapper())
        {
            
        }
        
        public async Task<DetailStoreProduct> FirstOrDefaultAsyncSanitized(string detailStoreProductId, object? userId = null, bool noTracking = true)
        {
            var detailStoreProduct = await UOW.DetailStoreProducts.FirstOrDefaultAsync(detailStoreProductId, userId, noTracking);
            var mappedDetailStoreProduct = Mapper.Map(detailStoreProduct);
            Sanitize(mappedDetailStoreProduct);
            return mappedDetailStoreProduct!;
        }

        public async Task<IEnumerable<DetailStoreProductAvailability>> GetAllProductAvailabilitiesAsync(object? userId = null, bool noTracking = true)
        {
            var availabilityMapper = new DetailStoreProductAvailabilityMapper();
            var detailStoreProducts = await UOW.DetailStoreProducts.GetAllAsync(userId, noTracking);
            return detailStoreProducts.Select(prod => availabilityMapper.Map(prod));
        }

        private static void Sanitize(DetailStoreProduct detailStoreProduct)
        {
            if (detailStoreProduct != null)
            {
                detailStoreProduct.Description = detailStoreProduct.Description.Replace("<", "&lt;").Replace(">", "&gt;")
                    .Replace("&lt;b&gt;", "<b>").Replace("&lt;/b&gt;", "</b>").Replace("&lt;i&gt;", "<i>").Replace("&lt;/i&gt;", "</i>");
            }
        }
    }
}