using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using ee.itcollege.chrrak.BLL.Base.Services;


namespace BLL.App.Services
{
    public class ListStoreProductService : BaseEntityService<string, IAppUnitOfWork, IListStoreProductRepository, IListStoreProductServiceMapper, DAL.App.DTO.ListStoreProduct, BLL.App.DTO.ListStoreProduct>, IListStoreProductService
    {
        public ListStoreProductService(IAppUnitOfWork uow) : base(uow, uow.ListStoreProducts, new ListStoreProductServiceMapper())
        {
        }

        public async Task<IEnumerable<ListStoreProduct>> GetAllAsyncAsyncSanitized(object? userId = null, bool noTracking = true)
        {
            var listStoreProducts = await UOW.ListStoreProducts.GetAllAsync(userId, noTracking);

            var mappedListStoreProducts = listStoreProducts.Select(e => Mapper.Map(e)).ToList();
            Sanitize(mappedListStoreProducts);

            return mappedListStoreProducts;
        }
        
        private static void Sanitize(IEnumerable<ListStoreProduct> listStoreProducts)
        {
            foreach (var listStoreProduct in listStoreProducts)
            {
                if (listStoreProduct != null)
                {
                    listStoreProduct.Description = listStoreProduct.Description.Replace("<", "&lt;").Replace(">", "&gt;")
                        .Replace("&lt;b&gt;", "<b>").Replace("&lt;/b&gt;", "</b>").Replace("&lt;i&gt;", "<i>").Replace("&lt;/i&gt;", "</i>");
                }
            }
        }
    }
}